# Based on winenr  https://github.com/CEA-LIST/Basket-Ball-Size-Estimation/blob/main/dataset.py
# and https://github.com/DeepSportRadar/ball-3d-localization-challenge
# and https://github.com/gabriel-vanzandycke/deepsport and the scripts in this repo
import torch
import timm
import argparse

from mlworkflow import PickledDataset, TransformedDataset, FilteredDataset
from deepsport_utilities import import_dataset, InstantsDataset, deserialize_keys
from deepsport_utilities.transforms import DataExtractorTransform
from deepsport_utilities.ds.instants_dataset import BuildBallViews, ViewsDataset, DownloadFlags
from deepsport_utilities.ds.instants_dataset.views_transforms import AddBallAnnotation, AddBallSizeFactory, AddImageFactory
from deepsport_utilities.ds.instants_dataset.dataset_splitters import DeepSportDatasetSplitter


class BallPickleDataset():
    def __init__(self, pickle_folder):
        # Based data loader shown in https://github.com/CEA-LIST/Basket-Ball-Size-Estimation/blob/main/dataset.py
        ds = PickledDataset(pickle_folder)
        self.ds = ds
        self.keys = []
        self.imgs = []
        self.labels = []
        for key in self.ds.keys:
            self.keys.append(key)
            item = self.ds.query_item(key)
            img = torch.tensor(item.image).permute(2, 0, 1).float()
            self.imgs.append(img)
            self.labels.append(torch.tensor(item.ball.center).flatten())

    def __getitem__(self, idx):
        # Torch permute
        # https://pytorch.org/docs/stable/generated/torch.permute.html
        return self.imgs[idx], self.labels[idx]

    def __len__(self):
        return len(self.keys)


"""
The following code highly Adapted based on https://github.com/CEA-LIST/Basket-Ball-Size-Estimation/blob/main/model.py
"""

class PretrainModel(torch.nn.Module):
    def __init__(self, model_name):
        super(PretrainModel, self).__init__()
        # moblenet https://paperswithcode.com/lib/timm/mobilenet-v3#
        self.m = timm.create_model(model_name, pretrained=True)
        self.W = torch.nn.Linear(1000, 3)

    def forward(self, x):
        x = self.m(x)
        y = self.W(x)
        return y


def train(args):
    trainfrac = 0.3
    batch_size = 1
    numepoch = args.epoch
    learningrate = 1e-4
    ds = BallPickleDataset(args.train_file)
    ### Split dataset based on https://stackoverflow.com/questions/50544730/how-do-i-split-a-custom-dataset-into-training-and-test-datasets/50544887#50544887

    model = PretrainModel(args.model_name)
    score = torch.nn.L1Loss()
    optimizer = torch.optim.AdamW(model.parameters(), lr=learningrate)

    from torch.utils.data import DataLoader
    train_loader = DataLoader(ds,
                              batch_size=batch_size,
                              shuffle=True)
    from tqdm import tqdm
    print("training...")
    for i in range(numepoch):
        totalloss = 0
        for x, y in tqdm(train_loader):
            pred = model(x)
            loss = score(pred, y)
            totalloss += loss

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        print("epoch %d total loss %f" % (i, totalloss))
        if i % 5 == 0:
            torch.save(model.state_dict(), "model_{}_trainset_{}_epoch{}.pth".format(args.model_name, args.train_file, i))
    torch.save(model.state_dict(), "model_{}_trainset_{}_epoch{}.pth".format(args.model_name, args.train_file, (numepoch - 1)))
    return model


def test(args):
    model = PretrainModel(args.model_name)
    model.load_state_dict(torch.load(args.model_file))
    testset = BallPickleDataset(args.test_file)
    model.eval()

    from torch.utils.data import DataLoader
    test_loader = DataLoader(testset, batch_size=1, shuffle=False)
    losses = []
    score = torch.nn.L1Loss()
    import numpy as np
    for x, y in test_loader:
        pred = model(x)
        losses.append(score(pred, y).item())
    loss = np.mean(losses)
    print("Average loss on test set {}: {}".format(args.test_file, loss))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # Use th parse based on https://github.com/DeepSportRadar/ball-3d-localization-challenge/blob/main/scripts/generate_dataset.py
    parser.add_argument("--train_file", type=str, default="")
    parser.add_argument("--test_file", type=str, default="")
    parser.add_argument("--model_file", type=str, default="")
    parser.add_argument("--model_name", type=str, default="mobilenetv3_large_100")
    parser.add_argument("--epoch", type=int, default=10)
    args = parser.parse_args()
    print(args)

    if args.train_file != "":
        train(args)
    else:
        test(args)


## Related but not sure how to use https://pvcnn.mit.edu/assets/slides.pdf
## Pretrain large image https://stats.stackexchange.com/questions/326171/applying-pre-trained-convolutional-neural-nets-on-large-images
## Auto encoder https://medium.com/pytorch/implementing-an-autoencoder-in-pytorch-19baa22647d1
## 3D detection https://openaccess.thecvf.com/content_cvpr_2018/html/Xu_Multi-Level_Fusion_Based_CVPR_2018_paper.html
## https://github.com/POSTECH-CVLab/PyTorch-StudioGAN
