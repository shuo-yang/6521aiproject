# Based on https://github.com/gabriel-vanzandycke/deepsport/blob/main/scripts/prepare_ball_views_dataset.py

import argparse
import os
from tqdm.auto import tqdm
from mlworkflow import PickledDataset, FilteredDataset, TransformedDataset
from deepsport_utilities import import_dataset
from deepsport_utilities.ds.instants_dataset import InstantsDataset, DownloadFlags, ViewsDataset, BuildCameraViews, AddBallAnnotation
import random


def generate_set(args, preds, output_name):
    # The `dataset_config` is used to create each dataset item
    dataset_config = {
        "download_flags": DownloadFlags.WITH_IMAGE | DownloadFlags.WITH_CALIB_FILE | DownloadFlags.WITH_FOLLOWING_IMAGE,
        "dataset_folder": args.dataset_folder  # informs dataset items of raw files location
    }

    # Import dataset
    database_file = os.path.join(args.dataset_folder, "basketball-instants-dataset.json")
    ds = import_dataset(InstantsDataset, database_file, **dataset_config)

    # Transform the dataset of instants into a dataset of views for each camera
    ds = ViewsDataset(ds, view_builder=BuildCameraViews())

    # Add the 'ball' attribute to the views, a shortcut to the ball in the annotation list
    ds = TransformedDataset(ds, [AddBallAnnotation()])

    # Filter only views for which camera index is the one in which the ball was annotated
    # Filter based on a list of predicates
    for pred in preds:
        ds = FilteredDataset(ds, predicate=pred)

    # Save the working dataset to disk with data contiguously stored for efficient reading during training
    output_folder = args.output_folder or args.dataset_folder
    path = os.path.join(output_folder, output_name)
    PickledDataset.create(ds, path, yield_keys_wrapper=tqdm)
    print(f"Successfully generated {path}")


def split_pickle(args, pickle_filename):
    # Split into two sets
    ds = PickledDataset(pickle_filename)
    train_name = pickle_filename.split(".pickle")[0] + "train.pickle"
    test_name = pickle_filename.split(".pickle")[0] + "test.pickle"

    all_num = len(ds.keys.keys)
    test_num = all_num // 2
    testkeys = random.sample(ds.keys.keys, test_num)
    testds = FilteredDataset(ds, predicate=lambda k, v : k in testkeys)
    trainds = FilteredDataset(ds, predicate=lambda k, v : k not in testkeys)

    trainpath = os.path.join(args.output_folder, train_name)
    testpath = os.path.join(args.output_folder, test_name)
    PickledDataset.create(trainds, trainpath, yield_keys_wrapper=tqdm)
    PickledDataset.create(testds, testpath, yield_keys_wrapper=tqdm)

    print(f"Successfully create train set {trainpath} and test set {testpath} total{all_num} test {test_num}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""
        Creates an mlworkflow.PickledDataset file named "camera_with_ball_visible_views.pickle" containing pairs of ViewKey, View objects.
        The View objects contain full image (with calibration data) as captured by the Keemotion system.
        """)
    parser.add_argument("--dataset-folder", required=True, help="Basketball Instants Dataset folder")
    parser.add_argument("--output-folder", default="./", help="Folder in which specific dataset will be created. Defaults to `dataset_folder` given in arguments.")
    args = parser.parse_args()

    # First two predicates share the same
    predicates = [lambda k,v: k.camera == v.ball.camera,
                  lambda k,v: v.ball.visible == True,
                  lambda k,v: k.arena_label == "KS-FR-GRAVELINES"]

    generate_set(args, predicates, "ksfrgravelinesall.pickle")

    # First two predicates share the same
    predicates = [lambda k,v: k.camera == v.ball.camera,
                  lambda k,v: v.ball.visible == True,
                  lambda k,v: k.arena_label == "KS-FR-GRAVELINES",
                  lambda k,v: k.camera == 0]

    generate_set(args, predicates, "ksfrgravelines_camera0.pickle")


    predicates = [lambda k,v: k.camera == v.ball.camera,
                  lambda k,v: v.ball.visible == True,
                  lambda k,v: k.arena_label != "KS-FR-GRAVELINES"]

    generate_set(args, predicates, "ksfr_nongravelinesall.pickle")

    predicates = [lambda k,v: k.camera == v.ball.camera,
                  lambda k,v: v.ball.visible == True,
                  lambda k,v: k.arena_label != "KS-FR-GRAVELINES",
                  lambda k,v: k.camera == 0]

    generate_set(args, predicates, "ksfr_nongravelines_camera0.pickle")


    split_pickle(args, "ksfrgravelinesall.pickle")
    split_pickle(args, "ksfrgravelines_camera0.pickle")

    test_arenas = ["KS-FR-CAEN", "KS-FR-ROANNE", "KS-FR-LIMOGES"]

    predicates = [lambda k,v: k.camera == v.ball.camera,
                  lambda k,v: v.ball.visible == True,
                  lambda k,v: k.arena_label in test_arenas,
                  lambda k,v: k.camera == 0]

    generate_set(args, predicates, "ksfr_crl_camera0.pickle")

    predicates = [lambda k,v: k.camera == v.ball.camera,
                lambda k,v: v.ball.visible == True,
                lambda k,v: k.arena_label not in test_arenas,
                lambda k,v: k.camera == 0]

    generate_set(args, predicates, "ksfr_non_crl_camera0.pickle")


